<html>

<head>
    <title>Latihan Hari 1</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        <!-- FIRST NAME -->
        @csrf
        <label for="first_name">First Name :</label>
        <input type="text" name="first_name" required>
        <!-- FIRST NAME  -->
        <br><br>

        <!-- LAST NAME -->
        <label for="last_name">Last Name :</label>
        <input type="text" name="last_name" required>
        <!-- LAST NAME -->
        <br><br>

        <!-- GENDER -->
        <label for="radio_gender">Gender : </label>
        <input type="radio" id="male" name="radio_gender" value="Male"> Male
        <input type="radio" id="female" name="radio_gender" value="Female"> Female
        <input type="radio" id="other" name="radio_gender" value="Other"> Other
        <!-- GENDER -->
        <br><br>

        <!-- NATIONALITY -->
        <label for="drop_nationality">Nationality : </label>
        <select name="drop_nationality" id="nationality">
            <option value="id">Indonesian</option>
            <option value="fr">French</option>
            <option value="de">German</option>
        </select>
        <!-- NATIONALITY -->
        <br><br>

        <!-- LANGUAGE -->
        <label for="language_check">Language Spoken : </label>
        <input type="checkbox" id="ind" name="language_check" value="ind"> Bahasa Indonesia
        <input type="checkbox" id="eng" name="language_check" value="eng"> English
        <input type="checkbox" id="oth" name="language_check" value="oth"> Other
        <!-- LANGUAGE -->
        <br><br>

        <!-- BIODATA -->
        <label for="biodata">Bio : </label><br>
        <textarea name="biodata" id="biodata" cols="55" rows="10"></textarea>
        <!-- BIODATA -->
        <br><br>

        <button type="submit">Sign Up</button>
    </form>
</body>

</html>